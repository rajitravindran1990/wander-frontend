FROM node:13.12.0-alpine
VOLUME /mnt
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 3030
CMD ["npm", "start"]