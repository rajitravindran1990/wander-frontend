export const RouteMap = {
    rootPage : '/',
    loginPage : '/login',
    signup : '/signup',
    dashboard : '/dashboard',
}

export const cookieKeys = {
    wanderToken : "wander-token",
}
