import React from 'react';
import CustomInput from "./CustomInput";
import '../assets/css/login.css';
import axios from 'axios'
import { Link } from 'react-router-dom';
import {RouteMap} from '../common/Constant';

class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email : '',
            password : '',
            name : '',
            loggedIn: false,
            message : '',
            loading: false
        }
        this.signup = this.signup.bind(this);
        this.onChange = this.onChange.bind(this);
        this.setErrorMessage = this.setErrorMessage.bind(this);
    }

    onChange(e){
        this.setState({...this.state , [e.target.name]:e.target.value});
    }

    setErrorMessage(msg) {
        this.setState ({message : msg});
    }

    signup() {
        if(this.state.email && this.state.password) {
            axios.post('http://18.216.234.114:8080/wander/signup', {
                name : this.state.name,
                email: this.state.email,
                password: this.state.password
            }, {
                withCredentials: true
            }).then(response => {
                if (response.data.success) {
                    this.setState({severity: 'success', open: true, message: response.data.message})
                } else {
                    this.setState({severity: 'error', open: true, message: response.data.message})
                }
            }).catch(error => {
                console.log(error)
            })
        } else {
            this.setErrorMessage("Email or password cannot be empty");
        }
    }

    render() {
        return (
            <div className="login-container gray-container">
                <ul>
                    <li><Link to={RouteMap.loginPage}>Login !!</Link></li>
                </ul>
                <div className="page-container" style={{minHeight:"393px"}}>
                    <div className="page-content">
                        <div className="content-wrapper">
                            <div className="content">
                                <form id="loginForm">
                                    <div className="card card-body login-form">
                                        <div className={'form-group'}>
                                            <CustomInput id={"name"} name={"name"} placeholder={"Name"} value={this.state.name} type={"text"} onChange={this.onChange}/>
                                        </div>
                                        <div className={'form-group'}>
                                            <CustomInput id={"email"} name={"email"} placeholder={"Email"} value={this.state.email} type={"text"} onChange={this.onChange}/>
                                        </div>
                                        <div className={'form-group'}>
                                            <CustomInput id={"password"} name={"password"} placeholder={"Password"} value={this.state.password} type={"password"} onChange={this.onChange}/>
                                        </div>
                                        <div className="form-group">
                                            <button onClick={this.signup} type="button" className="btn btn-danger btn-block legitRipple">
                                                Sign Up
                                            </button>
                                        </div>
                                        <div className="text-center mb-30">
                                            <p style={{color:"red"}}>{this.state.message}</p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    };
}



export default SignUp;
