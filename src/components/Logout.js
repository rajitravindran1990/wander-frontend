import React from 'react';
import '../assets/css/login.css';
import authClient from '../services/Auth'

class Logout extends React.Component {
    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
    }


    logout() {
        authClient.logout();
        //this.props.history.push("/login");
    }

    render() {
        return (
            <div className="login-container gray-container" style={{"float": "right"}}
                 onClick={this.logout}>
                <a href="#" className="btn btn-danger">
                    <span className="glyphicon glyphicon-log-out"></span> Log out
                </a>
            </div>
        );
    };
}


export default Logout;
