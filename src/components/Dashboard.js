import React from 'react'
import axios from 'axios'
import {connect} from 'react-redux'
import 'react-datepicker/dist/react-datepicker.css';
import Logout from "./Logout";
import authClient from '../services/Auth'
import Cookies from 'js-cookie';
import { MDBDataTableV5 } from 'mdbreact';

class Dashboard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            data: {
                columns : [
                    {"label":"Country","field":"country","sort":"asc"},
                    {"label":"Tested","field":"tested","sort":"asc"},
                    {"label":"Infected","field":"infected","sort":"asc"},
                    {"label":"Recovered","field":"recovered","sort":"asc"},
                    {"label":"Deceased","field":"deceased","sort":"asc"},
                    {"label":"LastUpdated","field":"lastUpdatedSource","sort":"asc"}
                ],
                rows : []
            },
            filteredRows: []
        }
    }

    componentDidMount() {
        let url = 'http://18.216.234.114:8080/wander/dashboard'
        axios.get(url, {
            headers: {
                'wander-token': Cookies.get('wander-token')
            }
        }).then(response => {
            if (response.data.success) {
                this.setState({data: response.data.body})
            }
        }).catch(error => {
            console.log(error);
        });
    }

    render() {

        return (
            <div>

                <table className={"table"}>
                    <tbody>
                    <tr>
                        <td style={{"border": "none"}}>
                            Hi, {authClient.getNameFromJWT()}!!
                        </td>
                        <td style={{"border": "none"}}>
                            <Logout style={{"float": "right"}}/>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <br/>
                <div className="container">
                    <MDBDataTableV5
                        scrollX={true}
                        maxHeight={"400px"}
                        scrollY={100}
                        bordered={true}
                        btn={true}
                        hover={true}
                        entries={10}
                        pagesAmount={4}
                        theadColor={"red"}
                        sortable={false}
                        data={this.state.data} />
                </div>
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        userId: state.userReducer['userId']
    }
}

export default connect(mapStateToProps)(Dashboard);