import React from 'react';
import CustomInput from "./CustomInput";
import '../assets/css/login.css';
import axios from 'axios'
import { Link } from 'react-router-dom';
import {RouteMap} from '../common/Constant';

class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email : '',
            password : '',
            loggedIn: false,
            message : '',
            loading: false
        }
        this.login = this.login.bind(this);
        this.onChange = this.onChange.bind(this);
        this.setErrorMessage = this.setErrorMessage.bind(this);
    }

    onChange(e){
        this.setState({...this.state , [e.target.name]:e.target.value});
    }

    setErrorMessage(msg) {
        this.setState ({message : msg});
    }

    login() {
        if(this.state.email && this.state.password){
            axios.post('http://18.216.234.114:8080/wander/login', {
                email: this.state.email,
                password: this.state.password
            }, {
                withCredentials: true
            }).then(response => {
                if (response.data.success) {
                    this.props.history.push("/dashboard");
                    this.setState({severity: 'success', open: true, message: response.data.message})
                } else {
                    this.setState({severity: 'error', open: true, message: response.data.message})
                }
            })
        } else {
            this.setErrorMessage("User name or password cannot be empty");
        }
    }

    render() {
        return (
            <div className="login-container gray-container">
                <ul>
                    <li><Link to={RouteMap.signup}>Sign Up!!</Link></li>
                </ul>
                <div className="page-container" style={{minHeight:"393px"}}>
                    <div className="page-content">
                        <div className="content-wrapper">
                            <div className="content">
                                <form id="loginForm">
                                    <div className="card card-body login-form">
                                        <div className={'form-group'}>
                                            <CustomInput id={"email"} name={"email"} placeholder={"Email"} value={this.state.email} type={"text"} onChange={this.onChange}/>
                                        </div>
                                        <div className={'form-group'}>
                                            <CustomInput id={"password"} name={"password"} placeholder={"Password"} value={this.state.password} type={"password"} onChange={this.onChange}/>
                                        </div>
                                        <div className="form-group">
                                            <button onClick={this.login} type="button" className="btn btn-danger btn-block legitRipple">
                                                Sign In
                                            </button>
                                        </div>
                                        <div className="text-center mb-30">
                                            <p style={{color:"red"}}>{this.state.message}</p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    };
}



export default LoginPage;
