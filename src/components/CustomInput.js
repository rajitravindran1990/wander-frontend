import React from "react";

const CustomInput = (props) => {
    return (
            <input id={props.id} name={props.name} className="form-control form-flat"
                   type={props.type} placeholder={props.placeholder} value={props.value} onChange={props.onChange} onKeyDown={props.onKeyPress}/>
    );
};

export default CustomInput;
