import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import authClient from './services/Auth';


const SecureRoute = ({component: Component, ...rest}) => (
    <Route {...rest}
           render={(props) => (
               authClient.isAuthenticated()
                   ? <div><Component {...props}/> </div>
                   : <Redirect to={
                       {
                           pathname: '/login',
                           state: { target: props.location }
                       }
                   } />
           )}
    />
);

export default SecureRoute;
