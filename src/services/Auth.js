import Cookie from 'cookie-universal';
import  {Secrets} from '../common/Secrets';
import {cookieKeys} from "../common/Constant";
import jwt from 'jwt-simple'

const cookies = Cookie();

function isAuthenticated() {
    let userId = getUserIdFromJWT();
    return userId > 0; //convert to boolean
}

function logout() {
    const c = cookies.get(cookieKeys.wanderToken);
    let d = new Date();
    d.setTime(d.getTime() - (24*60*60*1000));
    let expires = "expires="+ d.toUTCString();
    document.cookie = cookieKeys.wanderToken + "=" + c + ";" + expires + ";path=/";
}

function getUserIdFromJWT() {
    const c = cookies.get(cookieKeys.wanderToken);
    let user = null;
    try {
        user = jwt.decode(c, Secrets.jwtSecret, false, 'HS512')
        return user.id
    } catch (e) {
        //authentication failure
    }
    return -1;
}

function getNameFromJWT() {
    const c = cookies.get(cookieKeys.wanderToken);
    let user = null;
    try {
        user = jwt.decode(c, Secrets.jwtSecret, false, 'HS512')
        return user.name
    } catch (e) {
        //authentication failure
    }
    return "";
}


const authClient = {
    isAuthenticated, getUserIdFromJWT, logout, getNameFromJWT
};

export default authClient;
