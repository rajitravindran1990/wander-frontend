import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../src/assets/css/common.css';
import ReactDOM from 'react-dom';
import './index.css';
import { Provider } from 'react-redux'
import Routes from './Routes';
import configureStore from './redux/Store';
import * as serviceWorker from './serviceWorker';

const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <Routes/>
    </Provider>,
    document.getElementById('root')
);
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
