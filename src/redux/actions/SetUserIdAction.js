export function setUserIdAction(userId) {
    return {type: "SET_USER_ID", userId}
}