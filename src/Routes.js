import React from 'react';
import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom';
import {RouteMap} from './common/Constant';
import LoginPage from "./components/LoginPage";
import SignUp from "./components/SignUp";
import SecureRoute from "./SecureRoute";
import Dashboard from "./components/Dashboard";
import authClient from './services/Auth'

const Routes = () => (
    <Router>
        <Switch>
            <SecureRoute exact path={RouteMap.dashboard} component={Dashboard} />
            { authClient.isAuthenticated() && <Redirect from={RouteMap.rootPage} to={RouteMap.dashboard}/>}
            { authClient.isAuthenticated() && <Redirect from={RouteMap.signup} to={RouteMap.dashboard}/>}
            { authClient.isAuthenticated() && <Redirect from={RouteMap.loginPage} to={RouteMap.dashboard}/>}
            { !authClient.isAuthenticated() && <Redirect from={RouteMap.dashboard} to={RouteMap.loginPage}/>}
            <Route exact path={RouteMap.rootPage} component={LoginPage} />
            <Route exact path={RouteMap.loginPage} component={LoginPage} />
            <Route exact path={RouteMap.signup} component={SignUp} />
        </Switch>
    </Router>
);

export default Routes;
